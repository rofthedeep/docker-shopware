#!/bin/bash

#echo -n "phpmyadmin settings"
#if [ -f /etc/apache2/phpmyadmin.htpasswd ]
#then
#  HTPASSWD_OPTS=-Bbi
#else
#  HTPASSWD_OPTS=-cBbi
#fi
#
#if [ -n "$PHPMYADMIN_PW" ]; then
#    htpasswd -Bbc /etc/apache2/phpmyadmin.htpasswd phpmyadmin "${PHPMYADMIN_PW}"
#fi


# set ip address for xdebug
# sed -i "s/xdebug\.remote_host\=.*/xdebug\.remote_host\=$XDEBUG_HOST/g" /etc/php/7.0/apache2/conf.d/30-xdebug.ini


cat > /etc/phpmyadmin/config-db.php << EOF
<?php
\$dbuser='${DB_USER:-shopware}';
\$dbpass='${DB_PASSWORD:-shopware}';
\$basepath='';
\$dbname='${DB_DATABASE:-shopware}';
\$dbserver='${DB_HOST:-${DB_PORT_3306_TCP_ADDR}}';
\$dbport='${DB_PORT:-${DB_PORT_3306_TCP_PORT:-3306}}';
\$dbtype='mysql';
EOF

# enable xdebug
[ "$PHP_ENABLE_XDEBUG" = "true" ] && \
    docker-php-ext-enable xdebug && \
    echo "Xdebug is enabled"

# Configure Sendmail if required
if [ "$ENABLE_SENDMAIL" == "true" ]; then
    /etc/init.d/sendmail start
    echo "sendmail is configured"
fi

echo -n "apache foreground"
source /etc/apache2/envvars
exec apache2 -D FOREGROUND