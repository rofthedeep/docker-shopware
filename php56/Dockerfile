FROM ubuntu:14.04
MAINTAINER Tim Binder

ENV DEBIAN_FRONTEND noninteractive

# Need to generate our locale.
RUN locale-gen de_DE de_DE.UTF-8
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE.UTF-8

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    LANG=C.UTF-8 add-apt-repository ppa:ondrej/php5-5.6 && \
    apt-get update && \
    apt-get install -y \
    git \
    python \
    mercurial \
    vim \
    less \
    apache2 \
    libapache2-mod-python \
    syslog-ng-core \
    openssh-client \
    unzip \
    logrotate \
    cron \
    mysql-server \
    php5-xcache \
    supervisor \
    phpmyadmin \
    apache2-utils \
    php5-cli \
    pwgen \
    php5-gd \
    php-image-text \
    curl \
    php5-curl \
    nano

# configure apache
COPY files/apache-shopware.conf /etc/apache2/sites-available/000-default.conf
RUN sed --in-place "s/^upload_max_filesize.*$/upload_max_filesize = 10M/" /etc/php5/apache2/php.ini
RUN sed --in-place "s/^memory_limit.*$/memory_limit = 256M/" /etc/php5/apache2/php.ini

# configure vhost
ADD files/apache-shopware.conf /etc/apache2/sites-available/shopware.conf

RUN php -i | grep extension_dir

# install ioncube
ADD http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz /tmp/
RUN mkdir -p /usr/local/ioncube
WORKDIR /usr/local/
RUN tar xvf /tmp/ioncube_loaders_lin_x86-64.tar.gz
RUN cp ioncube/ioncube_loader_lin_5.6.so /usr/lib/php5/20131226/ioncube.so
RUN bash -c "echo zend_extension = /usr/lib/php5/20131226/ioncube.so > /etc/php5/apache2/conf.d/00-ioncube.ini"
RUN bash -c "echo zend_extension = /usr/lib/php5/20131226/ioncube.so > /etc/php5/cli/conf.d/00-ioncube.ini"

# Configure phpMyAdmin
COPY files/disable-advanced-usage.php /etc/phpmyadmin/conf.d/disable-advanced-usage.php
RUN ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf-enabled/phpmyadmin.conf

# enable mod rewrite
RUN a2enmod rewrite

# set volume
VOLUME ["/var/www/html"]

# copy entryoint file
COPY files/entrypoint.sh /entrypoint.sh

# set permissions for entrypoint.sh
RUN chmod +x /entrypoint.sh

# execute sh
ENTRYPOINT ["/entrypoint.sh"]

# Environment Port for VirtualHost / NginxService
ENV PORT 80

# Expose Port to port 80
EXPOSE 80

