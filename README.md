# Docker für Shopware

## log

### 06.02.2019

- add php7.2

### 25.07.2018

- corrected php limits
- working xdebug in php70 with fixed ip address
- new folder for ini files

### 23.07.2018

- renamed magerun to mr and mr2
- set php memory_limit to -1

### 15.07.2018

- added magerun 1/2 to php7.0
- added composer to php7.0
- added sendmail to php7.0
- added magerun2 to php7.1
- added composer to php7.1
- tried xdebug but doesn't work

### 12.07.2018

- add composer
- add xdebug
- add magerun
- add sw-cli

### 09.07.2018

- added php 7.1 Package
- removed ioncube loader, because it is not longer needed on shopware: [https://forum.shopware.com/discussion/50038/php7-1-mit-neuem-ioncube10/p2#quote](https://forum.shopware.com/discussion/50038/php7-1-mit-neuem-ioncube10/p2#quote)

### 25.09.2017

- added SSL Support
- Container Name: rotd/docker-shopware-php7-phpmyadmin-proxy-ioncube-ssl

### Sources

#### Magerun and xDebug

- [https://github.com/meanbee/docker-magento2/blob/master/7.0-fpm/Dockerfile](https://github.com/meanbee/docker-magento2/blob/master/7.0-fpm/Dockerfile)

### Build Image

On Command Line:

    docker build -t docker-image-name .
    -------
    sudo docker build -t rotd/docker-shopware-php56-phpmyadmin-proxy-ioncube-ssl:latest .
    sudo docker build -t rotd/docker-shopware-php70-phpmyadmin-proxy-ioncube-ssl:latest .
    sudo docker build -t rotd/docker-shopware-php71-phpmyadmin-proxy-ssl:latest .
    sudo docker build -t rotd/docker-shopware-php72-phpmyadmin-proxy-ssl:latest .


## Datenbankserver anlegen und konfigurieren

Sollten Sie einen bereits vorhandenen Datenbankserver verwenden, so müssen Sie die entsprechenden `DB_` Umgebungsvariablen einkommentieren und konfigurieren.
Diese Anleitung geht ab jetzt davon aus, dass Sie die Datenbank als dedizierten Docker-Container betreiben, womit beispielsweise die Anforderungen an die Passwörter geringer sind, da niemand sonst auf die Datenbank zugreifen kann.

Starten Sie den Datenbankserver mit

    docker-compose up -d shopwaredb


### Verbinden Sie sich mit dem Datenbankserver mit

    docker-compose run --rm dbclient

### Legen Sie eine Datenbank und einen Benutzer an:

    CREATE DATABASE shopware;
    GRANT ALL ON shopware.* TO 'shopware' IDENTIFIED BY 'shopware';
    exit

Der Text hinter `IDENTIFIED BY` ist das Passwort.
Sollten Sie ein anderes verwenden wollen, so müssen Sie beim Starten des Shopware-Containers die Umgebungsvariable `DB_PASSWORD` entsprechend setzen.


## Apache starten

Beim ersten Start von Shopware muss die Datenbank initialisiert werden.
Leider werden hierbei vom Shopware-Installer die Zugangsdaten zur Datenbank nicht übernommen, so dass man sie von Hand eingeben muss.
Starten Sie den shopware Container mit

    docker-compose up -d shopware


## phpMyAdmin

Um die Datenbank mit phpMyAdmin zu verwalten, muss die Umgebungsvariable `PHPMYADMIN_PW` gesetzt sein.
Wenn sie nicht gesetzt ist, besteht keine Möglichkeit, sich an phpMyAdmin erfolgreich anzumelden.
phpMyAdmin ist unter der URL [http://localhost/phpmyadmin/](http://localhost/phpmyadmin/) erreichbar.
Sollten Sie Docker nicht auf Ihrem Rechner betreiben, sondern auf einem anderen Server, so ersetzen Sie `localhost` durch den Namen oder die IP des Servers.

Bei dem Zugriff auf phpmyadmin werden Sie zuerst von Ihrem Browser nach Zugangsdaten gefragt.
Der Benutzername ist `phpmyadmin` und das Passwort ist das aus der Umgebungsvariable `PHPMYADMIN_PW`.
Danach werden Sie nach den Zugangsdaten zur `shopware` Datenbank gefragt.
Geben Sie für beides `shopware` ein, es sei denn, Sie haben etwas anderes mit `DB_PASSWORD` gesetzt.

User: phpmyadmin
Passwort: shopware

## Console aufrufen von docker container

    sudo docker exec -i -t 450a066dc7f4 /bin/bash
    
## PHP Pfad herausfinden

    php -i | grep extension_dir
    
## PHP Versiom

    php -v
    
## Datenbankoperationen

    show databases;
    SELECT User FROM mysql.user;

## Zugangsdaten Mysql

Diese finden sich in folgender Datei im shopware container:

    /etc/phpmyadmin/config-db.php

Z.B. folgendermaßen:

Falls man die IP nicht weis:

<pre>docker inspect containerid</pre>

	DB-Server: 172.17.0.2
	DB-Port: tcp://172.17.0.2:3306
	User: shopware
	Passwort: shopware
	Datenbank: shopware
	
## Alle Container anzeigen

    docker ps -a
    
## Docker Container Restart erzwingen

    docker-compose up -d --force-recreate shopware